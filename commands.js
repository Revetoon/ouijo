const Ping = require('./cmds/ping.js')
const Hi = require('./cmds/hi.js')
const Help = require('./cmds/help.js')
const Guesser = require('./cmds/guesser.js')
const {Perm} = require('./cmds/perm.js')
const Nani = require('./cmds/nani.js')
const Image = require('./cmds/image.js')


// Commands accessible by guild chat
const guildCommands = [Ping, Hi, Help, Guesser, Nani, Image]
// Commands accessible by direct message
const dmCommands = [Ping, Hi, Help, Perm]


function exec(msg) {
    var args = msg.content.substring(1).split(' ');
    var cmd = args[0].toLowerCase();
    var matched = false

    var instCmd = undefined

    var commands = (!msg.guild) ? dmCommands : guildCommands

    commands.forEach((command) => {
        if (command.name.toLowerCase() == cmd) {
            matched = true
            instCmd = new command(msg, args)
            instCmd.run()
        }
    })

    if (!matched) {
        instCmd = new Help(msg, args)
        instCmd.info()
    }
}


module.exports = {
    exec: exec,
    guildCommands: guildCommands,
    dmCommands: dmCommands
}