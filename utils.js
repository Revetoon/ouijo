function jsonEqual(a,b) {
        return JSON.stringify(a) === JSON.stringify(b);
    }


module.exports = {
    jsonEqual: jsonEqual
}