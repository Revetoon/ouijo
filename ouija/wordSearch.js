const info = require('../info.json')
const Fuse = require('fuse.js')
var dictEn = undefined
var dictFr = undefined
if (info.guesser) {
    dictEn = require('../data/dict/en.json')
    dictFr = require('../data/dict/fr.json')
    // var dictFrEn = require('../data/dict/fr_en.json')
}



function fuzzySearchWord(txt) {
    var options = {
        shouldSort: true,
        includeMatches: true,
        includeScore: true,
        threshold: 0.6,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: undefined
    };

    var fuseFr = new Fuse(dictFr, options);
    var fuseEn = new Fuse(dictEn, options);

    var resultFr = fuseFr.search(txt);
    var resultEn = fuseEn.search(txt);

    var res = { "lang": "XX", "guess": undefined,"word": txt}


    if (resultFr.length == 0 && resultEn.length == 0)
        return res


    if (resultEn.length == 0 || (resultFr.length != 0 && resultFr[0].score <= resultEn[0].score)) {
        res.guess = resultFr[0].matches[0].value
        res.lang = 'fr'
    } else {
        res.guess = resultEn[0].matches[0].value
        res.lang = 'en'
    }

    return res
}

module.exports = {
    fuzzySearchWord: fuzzySearchWord
}