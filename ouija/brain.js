var logger = require('winston');
const fs = require('fs');
const info = require('../info.json')
const {jsonEqual} = require('../utils.js')
const {fuzzySearchWord} = require('./wordSearch.js')
const messages = require('./messages.js')
const {resetInteractions} = require('./spamShield.js')


var currentWord = ""

//\\// history management

function appendHistory(result) {
    // update global variable
    global.lastGuess = result
    fs.appendFile(info.history, `${result.lang.toUpperCase()}|${result.word}|${result.guess}\n`, function (err) {
        if (err) throw err;
    });
}

//\\// process message

function specialMsg(msg) {
    if (messages.isEndSentence(msg.content)) {
        logger.info("end message received")
        if (currentWord.length > 1) {
            if (!info.guesser || !botGuess) {
                //guesser off
                msg.channel.send(`Ouija says: **${currentWord}**`)
            } else {
                // guesser on
                matchedWord = fuzzySearchWord(currentWord)
                appendHistory(matchedWord)
                if (matchedWord.guess === undefined) {
                    msg.channel.send(`Ouija says : **${currentWord}** 🤔`)
                } else if (currentWord.toLowerCase() == matchedWord.guess.toLowerCase()) {
                    msg.channel.send(`Ouija says: **${currentWord}**`)
                } else {
                    msg.channel.send(`Ouija says: **${currentWord}**\nDid you mean : ${matchedWord.guess} ?`)
                }
            }

            ouijoClient.user.setPresence({ game: { name: currentWord }, status: 'online' })

            // reset spam shield
            resetInteractions(msg.author.id)

        } else {
            msg.channel.send('What a little word this is ...')
        }
        // reset current word
        currentWord = ""
    } else {
        return false
    }
    return true
}

function msgType(msg) {
    if (messages.isEndSentence(msg.content)) {
        return
    }
}



function processMsg(msg) {
    if (!specialMsg(msg)) {
        if (msg.content.length > 1) {
            // long message processing
            if (messages.isComment(msg.content)) {
                logger.info('Comment: ' + msg.content)
                messages.setAsComment(msg)
            } else {
                // garbage
                msg.react('🚫')
                setTimeout(() => { messages.deleteMsg(msg) }, 5000);
            }
        } else {
            currentWord += msg.content
            logger.info(msg.content)
        }
    }
}

function processReaction(reaction, users) {
    console.log(reaction._emoji.name)

    if (reaction._emoji.name == '❌') {

        // if not admin => nothing
        var perm = 0
        for (let i = 0; i < users.length; ++i) {
            var user = {
                "username": users[i].username,
                "discriminator": users[i].discriminator,
                "userID": users[i].id
            }
            console.log(user)

            if (global.usersPerms.admin.find((u) => { console.log(u); return jsonEqual(u, user) }) !== undefined) {
                perm = 1
                break;
            }

        }
        if (perm == 1)
            messages.deleteMsg(reaction.message)
    }
}


module.exports = {
    processMsg: processMsg,
    processReaction: processReaction
}