var logger = require('winston');
const info = require('../info.json')


//\\// message management
function isComment(txt) {
    return txt.length > 2 && txt.endsWith('*') && txt.startsWith('*') && (txt.split("*").length - 1) == 2
}

function setAsComment(msg) {
    var commentEmoji = ouijoClient.emojis.find((emoji) => {
        return emoji.name == info.comment_emoji
    })
    if (commentEmoji === undefined) {
        msg.react('📚')
    } else {
        msg.react(msg.guild.emojis.get(commentEmoji.id))
    }
}

async function deleteMsg(msg) {
    msg.delete().then((msg) => {
        logger.info('deleted message: ' + msg.content)
    }).catch(logger.error)
}

function isEndSentence(txt) {
    return info.end_sentences.find((sentence) => {
        return sentence == txt
    }) !== undefined
}

module.exports = {
    isComment: isComment,
    setAsComment: setAsComment,
    deleteMsg: deleteMsg,
    isEndSentence: isEndSentence
}