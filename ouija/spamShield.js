const info = require('../info.json')
const {checkPerm} = require('../cmds/perm.js')

// interactions {
//     "1234567": {
//         "timestamp": 123123123,
//         "msgCount": 0
//         "consecutiveMsg": [12h23, 12h25],

//     }
// }


var interactions = {};
var msgCount = 0;
var lastAuthorID = 0;

function interact(msg) {

    // check for new user
    if (typeof interactions[msg.author.id] === 'undefined') {
        //new user
        console.log('new user')
        interactions[msg.author.id] = {
            "timestamp": 0,
            "msgCount" : 0,
            "consecutiveMsg": []
        }
    }

    // check for admin
    if (checkPerm(msg)) {
        ++interactions[msg.author.id]["msgCount"]
        interactions[msg.author.id]["timestamp"] = Date.now()
        return true
    }

    // timeout ceckout
    if (interactions[msg.author.id]['timestamp'] + info.timeout < Date.now()) {
        //can post message
        console.log('can post')
    } else {
        // cannot post (timeout)
        console.log('cannot post (timeout)')
        return false
    }

    // consecutive messages check
    if (lastAuthorID != 0 && lastAuthorID != msg.author.id) {
        resetInteractions(lastAuthorID)
    }
    else {
        // check for consecutive msgs
        var count = interactions[msg.author.id]["consecutiveMsg"].length
        while (count > 0) {
            if (interactions[msg.author.id]["consecutiveMsg"][count - 1] < Date.now())
                // removes the outdated ones
                interactions[msg.author.id]["consecutiveMsg"].pop()
            --count
        }

        // if interactions >= max
        if (interactions[msg.author.id]["consecutiveMsg"].length >= info.ouijaConsecutiveLetters) {
            // cannot post (too much messages)
            console.log('cannot post (too much messages)')
            return false
        }
    }

    console.log(Date.now(), interactions[msg.author.id])

    // update metadatas
    ++interactions[msg.author.id]["msgCount"]
    interactions[msg.author.id]["timestamp"] = Date.now()
    addInteraction(msg.author.id)
    lastAuthorID = msg.author.id

    return true
}

function addInteraction(author_id) {
    // add timestamp + timeout to the back of the array
    interactions[author_id]["consecutiveMsg"].push(Date.now() + info.ouijaConsecutiveTimeoutMinutes * 60000)
}

function resetInteractions(author_id) {
    interactions[author_id]["consecutiveMsg"] = []
}

module.exports = {
    interact: interact,
    resetInteractions : resetInteractions
}