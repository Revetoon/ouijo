# Ouija Bot [OUIJO]

Disclaimer: this is **not** the most beautiful piece of code there is, but it works fine 😃 .

First build the docker image
```
docker build -t ouijo .
```

Then create a service
```
docker service create --mode global --name ouijo ouijo
```
