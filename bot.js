const Discord = require('discord.js');
const client = new Discord.Client();
var logger = require('winston');
var auth = require('./auth.json');
var info = require('./info.json');
// const json = require('serialize-json')
var commands = require('./commands.js')
const {processMsg, processReaction} = require('./ouija/brain.js')
const {interact} = require('./ouija/spamShield.js')
const messages = require('./ouija/messages.js')
var fs = require("fs")
var perms = require('./perms.json')


logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';


global.usersPerms = perms
// fs.writeFileSync("perms.json", jsonData);


client.on('ready', () => {
    logger.info(`Logged in as ${client.user.tag}!`);
    global.ouijoClient = client;
});


client.on('message', msg => {

    // ignore own messages
    if (msg.author.username == client.user.username && msg.author.discriminator == client.user.discriminator) return;

    if (!msg.guild) { //\\//\\// Direct message commands
        if (msg.content.substring(0, 1) == '!') {
            commands.exec(msg, 'direct message')
        }
    }

    else { //\\//\\// Messages from a guild

        // wrong channel
        if (msg.channel.name != info.channel_name) return;

        // command or message
        if (msg.content.substring(0, 1) == '!') {
            commands.exec(msg)
        } else {

            //anti spam shield
            if (!interact(msg)) {
                msg.react('🚫')
                // delete message
                setTimeout(() => { messages.deleteMsg(msg) }, 5000);
                return
            }

            // process message
            processMsg(msg)
            // add 1 to message count for the user
        }
    }
});


client.on('messageReactionAdd', reaction => {

    // wrong channel
    if (reaction.message.channel.name != info.channel_name) return;

    var users = []
    console.log("reaction from : ")
    reaction.users.forEach((i) => {
        users.push(i)
        console.log(`${i.username}#${i.discriminator}`)
    })

    // ignore own messages
    // if (users.find((u) => {
    //     console.log(u.username, client.user.username); return `${u.username}#${u.discriminator}` === `${client.user.username}#${client.user.discriminator}`
    // }) !== undefined) return;

    console.log('we did it')

    processReaction(reaction, users)
});

client.login(auth.token);


global.botGuess = true;
