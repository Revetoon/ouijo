const assert = require('assert');

const {fuzzySearchWord} = require('../../ouija/wordSearch.js')

var test = ((text, result, lang) => {

    var expected = {
        "lang": lang,
        "guess": result,
        "word": text
    }

    assert.equal(JSON.stringify(fuzzySearchWord(text)), JSON.stringify(expected));
});


var items = [
    {
        "lang": "en",
        "guess": "abadejo",
        "word": "abcdef"
    },
    {
        "lang": "fr",
        "guess": "zigoto",
        "word": "zigoto"
    },
    {
        "lang": "en",
        "guess": "Parthenocissus",
        "word": "ociss"
    },
    {
        "lang": "fr",
        "guess": "écurèrent",
        "word": "écurèl"
    },
    {
        "lang": "fr",
        "guess": "djinn",
        "word": "êji"
    },
    {
        "lang": "fr",
        "guess": "laças",
        "word": "ças"
    },
]

items.forEach((i) => { test(i.word, i.guess, i.lang) } )
