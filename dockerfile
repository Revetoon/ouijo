FROM ubuntu

WORKDIR /home

RUN apt-get update && apt-get install -y git python3.6 python2.7 make autoconf automake gcc libtool gnupg2 gnupg1 g++
RUN apt-get install -y curl && curl -sL https://deb.nodesource.com/setup_11.x | bash - && \
    apt-get install -y nodejs


COPY package.json /home

RUN npm install uws opusscript node-opus libsodium-wrappers sodium

RUN npm install

COPY . /home

CMD ["node", "bot.js"]