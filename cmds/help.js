const Cmd = require('./cmd.js')
const info = require('../info.json')

class Help extends Cmd {
    constructor(msg, args) {
        super(msg, args)
    }

    run() {
        if (this.args.length != 1) {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return
        }

        const {guildCommands, dmCommands} = require('../commands.js')
        var commands = (!this.msg.guild) ? dmCommands : guildCommands

        this.help = `
# Help Ouija:
Concept: Write a word one letter at a time. Don\'t spam one letter message, the idea is to have multiple people involved in the writing.
Git repo: https://gitlab.com/Revetoon/ouijo

You are allowed to write:
    - one letter message
    - comment message following this format "*comment*"
    - word end indicator (${info["end_sentences"]})
    - commands (starting with "!")

available commands:
`
        commands.forEach((c) => {
            this.help += '  !' + c.name + '\n'
        })
        this.msg.channel.send(this.help, {code: 'md'})
    }

    info() {
        this.msg.channel.send('Type "!help" for help.')
    }

    usage() {
        console.log(module.exports)
        return `
Command: ${module.exports.name}

Show the help info.
`
    }
}


module.exports = Help;