const Cmd = require('./cmd.js')

class Ping extends Cmd {
    constructor(msg, args) {
        super(msg, args)
    }

    run() {
        if (this.args.length != 1) {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return
        }
        this.msg.reply('pong!')
    }

    usage() {
        return `
Command: ${module.exports.name}

Sends a ping to the bot.
`
    }
}


module.exports = Ping;