const Cmd = require('./cmd.js')

class Guesser extends Cmd {
    constructor(msg, args) {
        super(msg, args)
    }

    run() {
        const info = require('../info.json')
        const {checkPerm} = require('../cmds/perm.js')


        if (!info.guesser) {
            this.msg.react('❌')
            this.msg.channel.send("Guessing is disabled in the configs. It cannot be toggled by this command.")
            return
        }

        // check for perm
        if (!checkPerm(this.msg)) {
            this.msg.react('❌')
            this.msg.channel.send('`Permission needed.`', {code: 'md'})
            return
        }

        if (this.args.length != 2) {
            this.msg.react('❌')
            this.msg.channel.send(this.usage(), {code: 'md'})
            return
        }

        if (this.args[1].toLowerCase() == "on") {
            botGuess = true
            this.msg.react('✅')
        } else if (this.args[1].toLowerCase() == "off") {
            botGuess = false
            this.msg.react('✅')
        } else {
            this.msg.react('❌')
            this.msg.channel.send(this.usage(), {code: 'md'})
        }
    }

    usage() {
        return `
Command: ${module.exports.name} on|off

Toggle guessing when a word is completed.
`
    }

}


module.exports = Guesser;