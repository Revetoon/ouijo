const Cmd = require('./cmd.js')
var gis = require('g-i-s');
const {checkPerm} = require('./perm.js')
const info = require('../info.json')



class Image extends Cmd {
    constructor(msg, args) {
        super(msg, args)
    }

    run() {
        const info = require('../info.json')

        if (this.args.length == 2 && this.args[1] == 'help') {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return
        }

        // if word in history
        if (typeof lastGuess === 'undefined' || typeof lastGuess.guess === 'undefined' || !info.guesser) {
            this.msg.channel.send(this.noExplanations())
            return
        }

        // parse args
        const { queryStringAddition, count } = this.argParser()

        // if no perms, quit
        if (queryStringAddition === undefined)
            return


        var opts = {
            searchTerm: lastGuess.guess.toLowerCase(),
            queryStringAddition: queryStringAddition
        };

        // google image search
        gis(opts, (error, results) => {
            if (error) {
                console.log(error);
                this.msg.reply('An error occured :/.')
            }
            else {
                for (let i = 0; i < count; ++i) {
                    this.msg.channel.send(results[i].url)
                }
            }
        });


    }

    argParser() {
        // safe search arg
        var options = '&safe=active'
        var count = 1

        if (this.args.length != 1) {

            // check for perm
            if (!checkPerm(this.msg)) {
                this.msg.react('❌')
                this.msg.reply('Permission denied.')
                return { queryStringAddition: undefined, count: undefined}
            }
            // set image count
            if (this.args.length == 2 && !isNaN(parseInt(this.args[1], 10))) {
                count = parseInt(this.args[1], 10)
            }
            // unsafe
            else if (this.args.length >= 2 && this.args[1] == 'unsafe') {
                // set image count
                if (this.args.length == 3) {
                    if (isNaN(parseInt(this.args[2], 10)))
                        return { queryStringAddition: undefined, count: undefined}
                    else
                        count = parseInt(this.args[2], 10)
                }
                options = ''
            }
            // custom
            else if (this.args.length >= 3 && this.args[1] == 'custom') {
                // set image count
                if (this.args.length == 4) {
                    if (isNaN(parseInt(this.args[3], 10)))
                        return { queryStringAddition: undefined, count: undefined}
                    else
                        count = parseInt(this.args[3], 10)
                }
                options = this.args[2]
            }
        }
        if (count > info.maxImageRequest)
            count = info.maxImageRequest
        else if (count < 1)
            count = 1

        return { queryStringAddition: `${options}`, count: count}
    }

    noExplanations() {
        return 'No images available :/ .'
    }

    usage() {
        return `
Command: ${module.exports.name} [unsafe|CUSTOM_OPT] [COUNT]

When you need an image of a word. (Options may require permissions :p ).
With: 1 <= COUNT <= 5.
`
    }
}


module.exports = Image;