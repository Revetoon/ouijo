const Cmd = require('./cmd.js')
const {jsonEqual} = require('../utils.js')


class Perm extends Cmd {
    constructor(msg, args) {
        super(msg, args)
    }

    run() {
        if (this.args.length < 2) {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return
        }

        var fs = require("fs")

        var user = {
            "username": this.msg.author.username,
            "discriminator": this.msg.author.discriminator,
            "userID": this.msg.author.id
        }

        if (this.args[1] == 'admin') {
            if (!this.admin(user))
                return
        } else if (this.args[1] == 'list') {
            if (!this.list(user))
                return
        }

        this.msg.react('✅')
        fs.writeFileSync("perms.json", JSON.stringify(usersPerms));
    }


    list (user = undefined) {
        const {jsonEqual} = require('../utils.js')

        if (this.args.length != 2) {
            return false
        }

        if (global.usersPerms.admin.find((u) => { return jsonEqual(u, user) }) === undefined) {
            this.msg.reply('Permission denied.')
            return false
        }

        this.msg.channel.send(JSON.stringify(usersPerms), {code: 'json'})
        return true
    }

    admin(user) {
        var logger = require('winston');

        const {adminPwd} = require('../auth.json')

        if (this.args.length < 3) {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return false
        }

        if (this.args[2] == 'add') { //\\// Add new admin

            // wrong password check
            if (this.args.length != 4 || this.args[3] != adminPwd) {
                logger.info('wrong password')
                this.msg.reply('Wrong password.')
                return false
            }

            if (global.usersPerms.admin.find((u) => { return jsonEqual(u, user) }) !== undefined) {
                // already exists
                logger.info('already exists')
                this.msg.reply('The user is already in the list.')
                return false
            }
            usersPerms.admin.push(user)

        } else if (this.args[2] == 'rm') { //\\// Remove admin

            console.log(user)
            var index = -1;
            if (global.usersPerms.admin.find((u) => { ++index; return jsonEqual(u, user) }) === undefined) {
                // already exists
                logger.info('does not exist')
                this.msg.reply('The user is not in the list.')
                return false
            }
            if (index > -1) {
                usersPerms.admin.splice(index, 1);
            }
        } else {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return false
        }



        return true
    }


    usage() {
        return `
Command: Perm

Manage permissions.

    Perm admin add pwd    Add yourself to the admin list
    Perm admin rm         remove yourself from the admin list
    Perm list             List permissions
`
    }
}

function checkPerm(msg) {
    var user = {
        "username": msg.author.username,
        "discriminator": msg.author.discriminator,
        "userID": msg.author.id
    }

    if (global.usersPerms.admin.find((u) => { return jsonEqual(u, user) }) === undefined) {
        return false
    }
    return true
}

module.exports = { Perm, checkPerm };