const Cmd = require('./cmd.js')

class Hi extends Cmd {
    constructor(msg, args) {
        super(msg, args)
    }

    run() {
        if (this.args.length != 1) {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return
        }
        this.msg.channel.send('Hi ' + this.msg.author.username + '! :)')
    }

    usage() {
        return `
Command: ${module.exports.name}

Say hi to the bot.
`
    }
}


module.exports = Hi;