const Cmd = require('./cmd.js')

class Nani extends Cmd {
    constructor(msg, args) {
        super(msg, args)
    }

    run() {
        const info = require('../info.json')

        if (this.args.length != 1) {
            this.msg.channel.send(this.usage(), {code: 'md'})
            return
        }

        if (typeof lastGuess === 'undefined' || typeof lastGuess.guess === 'undefined' || !info.guesser) {
            this.msg.channel.send(this.noExplanations())
            return
        }

        var wikiAddress = `https://${lastGuess.lang.toLowerCase()}.wiktionary.org/wiki/${lastGuess.guess.toLowerCase()}`
        var translateAddress = `http://www.wordreference.com/${lastGuess.lang.toLowerCase() == 'en' ? 'enfr' : 'fren'}/${lastGuess.guess.toLowerCase()}`

        this.msg.channel.send(this.explanations(wikiAddress))
        this.msg.channel.send(this.translation(translateAddress))
    }

    explanations(wikiAddress) {
        return `${(lastGuess.lang == 'en') ? '🇬🇧' : '🇫🇷'} **${lastGuess.guess}**: ${wikiAddress}`
    }

    translation(translateAddress) {
        return `${(lastGuess.lang == 'en') ? '➡️ 🇫🇷' : '➡️ 🇬🇧'} ${translateAddress}`
    }

    noExplanations() {
        return 'No explanations available :/ .'
    }

    usage() {
        return `
Command: ${module.exports.name}

When you need explanations or translation on a word.
`
    }
}


module.exports = Nani;